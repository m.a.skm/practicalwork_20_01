// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGame_001GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_001_API ASnakeGame_001GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
